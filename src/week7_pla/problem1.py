import sys
import csv
#import matplotlib.pyplot as plt
import random


#import matplotlib.pyplot as plt
#from matplotlib.lines import Line2D


def importData(inputFile):
    with open(inputFile) as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        X1 = []
        X2 = []
        labels = []
        for row in readCSV:
            X1.append(int(row[0]))
            X2.append(int(row[1]))
            if int(row[2]) == 1:
                labels.append(int(row[2]))
            else:
                labels.append(0)
            
        return X1, X2, labels
    csvfile.close()

"""
# Currently the line doesn't extend completely. It may be possible to do so Ref: https://stackoverflow.com/questions/19310735/extending-a-line-segment-in-matplotlib    
def showPlot(X1,X2, labels, W):
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.scatter(X1, X2, c=labels)
    
    a1 = 0
    a2 = -(W[0])/W[2]

    a3 = -(W[0])/W[1]
    a4 = 0
    
    xPoints = [a1,a3]
    yPoints = [a2,a4]
    
    print(str(a1) + "," + str(a3))
    print(str(a2) + "," + str(a4))
    
    #Ref: https://stackoverflow.com/questions/28688210/use-line2d-to-plot-line-in-python
    line = Line2D(xPoints, yPoints, color='red')
    ax.add_line(line)
    
    plt.show()
"""    

def initializeParameters():
    #TODO: how to initialize a random shaped matrix? np.zeros? 
    #the lecture says these should be zeros, but then nothing will happen. We must initialize to non-zero values
    #Ref: https://www.youtube.com/watch?v=_tMpeah8XGQ..initialize to small non-zero values
    #Ref: https://stackoverflow.com/questions/33359740/random-number-between-0-and-1-in-python
    W =[random.randint(0, 10),random.randint(0, 10), random.randint(0, 1)]
    return W

def writeOutputFile(W, file):
    #Ref: https://www.pythonforbeginners.com/files/reading-and-writing-files-in-python
    line = str(W[1]) + "," + str(W[2]) + "," + str(W[0]) + "\n"
    file.write(line)

def runPLA(X1,X2,labels,W, learningRate, outputFile):
    file = open(outputFile,"w")
    
    while True:
        #The AIMA book says that this is applied stochastically to one input at a time randomly..however the video says that we sum over errors, which makes more sense
        #?? generate random number to select a random X,Y value as per the algorithm
        #Ref: https://www.youtube.com/watch?v=4aksMtJHWEQ
        totalError = 0
        for i in range(0, len(X1)):
            WX = W[0] + W[1]*X1[i] + W[2]*X2[i]
            
            HWX = None
            #This is as per lecture notes/problem guidelines
            if WX > 0:
                HWX = 1
            else:
                HWX = 0
            
            cost = labels[i] - HWX
            
            W[0] = W[0] + learningRate*cost
            W[1] = W[1] + learningRate*cost*X1[i]
            W[2] = W[2] + learningRate*cost*X2[i]
            
            totalError = totalError + cost*cost;
            
            
        writeOutputFile(W, file)
        
        if totalError == 0:
            break
            
    
    file.close()
    
    return W


inputFile = sys.argv[1]
outputFile = sys.argv[2]

X1,X2 ,labels = importData(inputFile)

#showPlot(X1, X2, labels)

W = initializeParameters()

learningRate = .005
W = runPLA(X1,X2,labels, W, learningRate, outputFile)

#showPlot(X1, X2, labels, W)