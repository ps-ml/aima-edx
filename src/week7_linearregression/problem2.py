"""
The input provides mapping between weight/age and height. I believe the task is to find the weights which allow us to predict
height for arbitrary weight/age values
"""

import sys
import csv
import numpy as np
import random

def importData(inputFile):
    with open(inputFile) as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        AGE = []
        WEIGHT = []
        HEIGHT = []
        
        for row in readCSV:
            #https://stackoverflow.com/questions/482410/how-do-i-convert-a-string-to-a-double-in-python..python floats are C doubles
            #TODO: Can I not create np array here?
            AGE.append(float(row[0]))
            WEIGHT.append(float(row[1]))
            HEIGHT.append(float(row[2]))

        return np.asarray(AGE), np.asarray(WEIGHT), np.asarray(HEIGHT)
    csvfile.close()

def scaleFeature(FEATURE):
    #https://stackoverflow.com/questions/7716331/calculating-arithmetic-mean-one-type-of-average-in-python
    #per project instructions it is ok to use numpy
    meanFEATURE = np.mean(FEATURE)
    stdevFEATURE = np.std(FEATURE)
    
    for i in range(len(FEATURE)):
        FEATURE[i] = (FEATURE[i] - meanFEATURE)/stdevFEATURE
    
    
    return FEATURE

def initializeParameters():
    W =[0,0,0]
    return np.asarray(W)


def calculateCost(AGE, WEIGHT, HEIGHT, W):
    totalCost = 0
    for i in range( len(AGE) ):
        YHAT = W[0] + W[1]*AGE[i] + W[2]*WEIGHT[i]
        cost = YHAT - HEIGHT[i]
        totalCost = totalCost + cost*cost
    
    totalCost = totalCost/(2*len(AGE))
    return totalCost
    
def createInputMatrix(AGE,WEIGHT):
    
    bias = np.ones((len(AGE)))
    #NPAGE = np.asarray(AGE)
    #NPWEIGHT = np.asarray(WEIGHT)
    
    X = np.column_stack((bias,AGE,WEIGHT))
    return X


def gradientDescent( AGE, WEIGHT, HEIGHT, W, learningRate, num_iters):
    X = createInputMatrix(AGE,WEIGHT)
    m = HEIGHT.shape[0]
    
    for i in range(num_iters):
        prediction = np.dot(X,W)
        cost = np.subtract(prediction,HEIGHT )
        
        jthetaPartialDifferential = []
        
        for j in range( X.shape[1]):
            costX = np.multiply(cost,X[:,j])
            sumcostX = np.sum(costX)
            jthetaPartialDifferential.append(sumcostX/m)

        jthetaPartialDifferential = np.asarray(jthetaPartialDifferential)    
        
        jthetaPartialDifferential = np.multiply(learningRate, jthetaPartialDifferential)
        
        W = np.subtract(W, jthetaPartialDifferential)  
        
    return W
    
def writeOutputFile(alpha, number_of_iterations, W):
    #Ref: https://www.pythonforbeginners.com/files/reading-and-writing-files-in-python
    line = str(alpha) + "," + str(number_of_iterations) + "," + str(W[0]) + "," + str(W[1]) + "," + str(W[2]) + "\n"
    file.write(line)    
    
        

inputFile = sys.argv[1]
outputFile = sys.argv[2]

AGE, WEIGHT, HEIGHT = importData(inputFile)

m = len(AGE)

AGE = scaleFeature(AGE)
WEIGHT = scaleFeature(WEIGHT)
#HEIGHT = scaleFeature(HEIGHT)

W = initializeParameters()

learningRates = [0.001, 0.005, 0.01, 0.05, 0.1, 0.5, 1, 5, 10,0.3]

num_iters = 100
"""
W = gradientDescent(AGE, WEIGHT, HEIGHT, W, learningRates[5], num_iters)
print(HEIGHT)
X = createInputMatrix(AGE,WEIGHT)
print(np.dot(X,W))
"""

file = open(outputFile,"w")
for i in range(len(learningRates)):
    W_new = gradientDescent(AGE, WEIGHT, HEIGHT, W, learningRates[i], num_iters)
    writeOutputFile(learningRates[i], num_iters, W_new)
    #print(calculateCost(AGE, WEIGHT, HEIGHT, W))
file.close()    

"""
X = createInputMatrix(AGE, WEIGHT)
print(HEIGHT)
print( np.dot(X,W) )
"""