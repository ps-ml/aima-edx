import os
import subprocess
import sys

def deleteFile(filename):
    try:
        os.remove(filename)
    except OSError:
        pass

deleteFile("output.txt")

#Ref: https://stackoverflow.com/questions/3277503/in-python-how-do-i-read-a-file-line-by-line-into-a-list
#Ref: https://stackoverflow.com/questions/4060221/how-to-reliably-open-a-file-in-the-same-directory-as-a-python-script
lines = [line.rstrip('\n') for line in open(os.path.join(sys.path[0], 'sudokus_start.txt'))]

for line in lines:
    #Ref: https://stackoverflow.com/questions/5788891/execute-a-file-with-arguments-in-python-shell
    subprocess.call("python driver.py " + line, shell=True)
    