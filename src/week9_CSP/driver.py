import sys
from collections import OrderedDict

#row and column identifiers
letters = ["A", "B", "C", "D", "E", "F", "G", "H", "I"]
numbers = ["1", "2", "3", "4", "5", "6", "7", "8", "9"]

rowToCellMap = {
                "A":["A1","A2","A3","A4","A5","A6","A7","A8","A9"],
                "B":["B1","B2","B3","B4","B5","B6","B7","B8","B9"],
                "C":["C1","C2","C3","C4","C5","C6","C7","C8","C9"],
                "D":["D1","D2","D3","D4","D5","D6","D7","D8","D9"],
                "E":["E1","E2","E3","E4","E5","E6","E7","E8","E9"],
                "F":["F1","F2","F3","F4","F5","F6","F7","F8","F9"],
                "G":["G1","G2","G3","G4","G5","G6","G7","G8","G9"],
                "H":["H1","H2","H3","H4","H5","H6","H7","H8","H9"],
                "I":["I1","I2","I3","I4","I5","I6","I7","I8","I9"],
    
                }

columnToCellMap = {
                    "1":["A1","B1","C1","D1","E1","F1","G1","H1","I1"],
                    "2":["A2","B2","C2","D2","E2","F2","G2","H2","I2"],
                    "3":["A3","B3","C3","D3","E3","F3","G3","H3","I3"],
                    "4":["A4","B4","C4","D4","E4","F4","G4","H4","I4"],
                    "5":["A5","B5","C5","D5","E5","F5","G5","H5","I5"],
                    "6":["A6","B6","C6","D6","E6","F6","G6","H6","I6"],
                    "7":["A7","B7","C7","D7","E7","F7","G7","H7","I7"],
                    "8":["A8","B8","C8","D8","E8","F8","G8","H8","I8"],
                    "9":["A9","B9","C9","D9","E9","F9","G9","H9","I9"],
                    }


#helper maps which help us quickly identify which box a cell belongs to and vice versa
boxToCellMap = {"1":["A1","A2","A3","B1","B2","B3","C1","C2","C3"],
                "2":["A4","A5","A6","B4","B5","B6","C4","C5","C6"],
                "3":["A7","A8","A9","B7","B8","B9","C7","C8","C9"],
                "4":["D1","D2","D3","E1","E2","E3","F1","F2","F3"],
                "5":["D4","D5","D6","E4","E5","E6","F4","F5","F6"],
                "6":["D7","D8","D9","E7","E8","E9","F7","F8","F9"],
                "7":["G1","G2","G3","H1","H2","H3","I1","I2","I3"],
                "8":["G4","G5","G6","H4","H5","H6","I4","I5","I6"],
                "9":["G7","G8","G9","H7","H8","H9","I7","I8","I9"]
                }

#map which stores which row/column/box a cell belongs to
#https://stackoverflow.com/questions/2853683/what-is-the-preferred-syntax-for-initializing-a-dict-curly-brace-literals-or
cellToContainerMap = {    
                    "A1": ["A","1","1"],
                    "A2": ["A","2","1"],
                    "A3": ["A","3","1"],
                    "A4": ["A","4","2"],
                    "A5": ["A","5","2"],
                    "A6": ["A","6","2"],
                    "A7": ["A","7","3"],
                    "A8": ["A","8","3"],
                    "A9": ["A","9","3"],
                    "B1": ["B","1","1"],
                    "B2": ["B","2","1"],
                    "B3": ["B","3","1"],
                    "B4": ["B","4","2"],
                    "B5": ["B","5","2"],
                    "B6": ["B","6","2"],
                    "B7": ["B","7","3"],
                    "B8": ["B","8","3"],
                    "B9": ["B","9","3"],
                    "C1": ["C","1","1"],
                    "C2": ["C","2","1"],
                    "C3": ["C","3","1"],
                    "C4": ["C","4","2"],
                    "C5": ["C","5","2"],
                    "C6": ["C","6","2"],
                    "C7": ["C","7","3"],
                    "C8": ["C","8","3"],
                    "C9": ["C","9","3"],
                    "D1": ["D","1","4"],
                    "D2": ["D","2","4"],
                    "D3": ["D","3","4"],
                    "D4": ["D","4","5"],
                    "D5": ["D","5","5"],
                    "D6": ["D","6","5"],
                    "D7": ["D","7","6"],
                    "D8": ["D","8","6"],
                    "D9": ["D","9","6"],
                    "E1": ["E","1","4"],
                    "E2": ["E","2","4"],
                    "E3": ["E","3","4"],
                    "E4": ["E","4","5"],
                    "E5": ["E","5","5"],
                    "E6": ["E","6","5"],
                    "E7": ["E","7","6"],
                    "E8": ["E","8","6"],
                    "E9": ["E","9","6"],
                    "F1": ["F","1","4"],
                    "F2": ["F","2","4"],
                    "F3": ["F","3","4"],
                    "F4": ["F","4","5"],
                    "F5": ["F","5","5"],
                    "F6": ["F","6","5"],
                    "F7": ["F","7","6"],
                    "F8": ["F","8","6"],
                    "F9": ["F","9","6"],
                    "G1": ["G","1","7"],
                    "G2": ["G","2","7"],
                    "G3": ["G","3","7"],
                    "G4": ["G","4","8"],
                    "G5": ["G","5","8"],
                    "G6": ["G","6","8"],
                    "G7": ["G","7","9"],
                    "G8": ["G","8","9"],
                    "G9": ["G","9","9"],
                    "H1": ["H","1","7"],
                    "H2": ["H","2","7"],
                    "H3": ["H","3","7"],
                    "H4": ["H","4","8"],
                    "H5": ["H","5","8"],
                    "H6": ["H","6","8"],
                    "H7": ["H","7","9"],
                    "H8": ["H","8","9"],
                    "H9": ["H","9","9"],
                    "I1": ["I","1","7"],
                    "I2": ["I","2","7"],
                    "I3": ["I","3","7"],
                    "I4": ["I","4","8"],
                    "I5": ["I","5","8"],
                    "I6": ["I","6","8"],
                    "I7": ["I","7","9"],
                    "I8": ["I","8","9"],
                    "I9": ["I","9","9"]
                }



#this method convers an input sudoku string into a python dict where key is a row+column identifier, e.g, A1-I9    
def getDict(input_string):
    #https://stackoverflow.com/questions/1514553/how-to-declare-an-array-in-python
    count = 1;
    sudokuDict = OrderedDict()
    input_stringList = list(input_string)
    
    for i in range (len(letters)):
        for j in range(len(numbers)):
            key = letters[i] + numbers[j]
            valueIndex = 9*i + j
            if input_stringList[valueIndex] != "0":
                sudokuDict[key] = [input_stringList[valueIndex]]
            else:
                sudokuDict[key] = numbers
    
    return sudokuDict

#This generates the intial queue ( which is implemented as a dict ), This queue maintains the mapping of all zero valued variable names to their domains (initially 1-9 which are the possible values that these variables can take.). In the revise function we try to resduce the values in these domains
def getArcQueue(sudokuDict):
    arcQueueDict = OrderedDict()
    for key, value in sudokuDict.iteritems():
        if len(value) != 1:
            arcQueueDict[key] = numbers
    
    return arcQueueDict

#check if this arc has unique elements except zeros. An arc could be a sudoko row, column or box of 9 elements
def AllDiff(arc):
    for i in numbers:
        #Ref: https://stackoverflow.com/questions/20167108/how-to-check-how-many-times-an-element-exists-in-a-list
        if arc.count(i) > 1:
            return False
    
    return True

#checks if a current sudoku satisfies the sudoku constraints
# it can probably be optimized to check only the row/column/box where the value changed
def isSudokuConsistent(sudokuDict):
    #check rows
    for i in range (len(letters)):
        #row, column and box can't be a set because at least initially they will contain multiple zeros
        row = []
        for j in range(len(numbers)):
            key = letters[i] + numbers[j]
            #we need to check only those values in the row which have single values
            if len(sudokuDict[key]) == 1:
                row.append(sudokuDict[key][0])
            
        if not AllDiff(row):    
            return False
    
    #add all columns to arcQueue    
    for i in range (len(numbers)):
        column = []
        for j in range(len(letters)):
            key = letters[j] + numbers[i]
            #we need to check only those values in the column which have single values
            if len(sudokuDict[key]) == 1:
                column.append(sudokuDict[key][0])
    
        if not AllDiff(column):    
            return False
                
        
    
    #add all boxes to arcQueue    
    for i in [0, 3, 6]:
        for j in [0, 3, 6]:
            krange_min = i
            krange_max = i + 3

            box = []            
            for k in range (krange_min,krange_max):
                lrange_min = j
                lrange_max = j + 3
                
                for l in range (lrange_min,lrange_max):
                    key = letters[k] + numbers[l]
                    #we need to check only those values in the box which have single values
                    if len(sudokuDict[key]) == 1:
                        box.append(sudokuDict[key][0])
        
            if not AllDiff(box):    
                return False 
            
    
    return True


#converts a sudokuDict to a string format. Useful, because we can then use this string as a key in a dict
def getStringFromDict(sudokuDict):
    sudokuString = ""
    for key in sorted(cellToContainerMap.iterkeys()):
        value = sudokuDict[key]
        if(len(value) == 1):
            sudokuString = sudokuString + value[0]
        else:
            sudokuString = sudokuString + "0"

    return sudokuString

#implementation of REVISE as I understood it and applies to the sudoku puzzle. This is not an 1-1 mapping from the algorithm in the book
def REVISE(cell, domain, sudokuDict):
    revised = False
    
    #Ref: https://stackoverflow.com/questions/2612802/how-to-clone-or-copy-a-list
    domainCopy = list(domain)
    
    #for each x in domain, find if x satisfies all constraints, if not we remove x from the domain
    for x in domain:
        #Ref: https://stackoverflow.com/questions/2465921/how-to-copy-a-dictionary-and-only-edit-the-copy
        newSudokuDict = sudokuDict.copy()
        newSudokuDict[cell] = x
        if not isSudokuConsistent(newSudokuDict):
            domainCopy.remove(x)
            revised = True
    
    return revised, domainCopy

#update queue as per the AC-3 algorithm
def updateQueue(cell, arcQueueDict, sudokuDict):
    cellCharacters = list(cell)
    
    for key, value in sudokuDict.iteritems():
        keyCharacters = list(key)
        #items are in the same row/column/box and thus neighbours and the domain size is greater than 1
        #Ref: https://stackoverflow.com/questions/2492087/how-to-get-the-nth-element-of-a-python-list-or-a-default-if-not-available
        if ( keyCharacters[0] == cellCharacters[0] or keyCharacters[1] == cellCharacters[1] or cellToContainerMap[cell][2] == cellToContainerMap[key][2] ) and len(value) > 1:
            arcQueueDict[key] = value
    
    return  arcQueueDict

#implementation of AC3 algorithm    
def AC3(arcQueueDict, sudokuDict):
    
    while( len(arcQueueDict) != 0 ):
        #pick an item from the dict
        #https://stackoverflow.com/questions/10058140/accessing-items-in-a-ordereddict 
        cell, domain =  arcQueueDict.iteritems().next()
        
        #also remove the key from the queue
        #https://stackoverflow.com/questions/11277432/how-to-remove-a-key-from-a-python-dictionary
        arcQueueDict.pop(cell)
        
        revised, domain = REVISE(cell, domain, sudokuDict)
        
        if revised:
            if len(domain) == 0:
                return False
            
            #update the domain based on updated values
            sudokuDict[cell] = domain
            
            #add neighbours to the queue..meaning all rows and columns and boxes
            arcQueueDict = updateQueue(cell, arcQueueDict, sudokuDict)
    
    return isSolutionValid(sudokuDict)

def isSolutionValid(assignment):
    #print("isSolutionValid: " + getStringFromDict(assignment))
    if not isSudokuConsistent(assignment):
        return False
    
    #maybe this check should be first?
    for key, value in assignment.iteritems():
        if len(value) != 1:
            return False
    
    return True 

def squeezeArc(key, arc, assignment):
    singleValues = set(())
    
    for arcKey in arc:
        #if after multiple squeezing the current key is already length =1, then we don't want to add it to singlevalues set, else when we calcualted domain complement we will end up removing it also
        if arcKey != key and len(assignment[arcKey]) == 1:
            singleValues.add(assignment[arcKey][0])
    
    #Ref: https://stackoverflow.com/questions/15768757/how-to-construct-a-set-out-of-list-items-in-python
    domainSet = set(assignment[key])
    
    #Ref: https://stackoverflow.com/questions/6486450/python-compute-list-difference
    squeezedArc = list(domainSet - singleValues)
   
    return squeezedArc
    

#squeeze domain values
def squeezeDomains(assignment):
    for key, value in assignment.iteritems():
        #only cells which have multiple possible values can be squeezed
        if len(value) > 1:
            #get all cells in the row this cell belongs to
            row =  rowToCellMap[list(key)[0]]
            assignment[key] = squeezeArc(key, row, assignment)
            column = columnToCellMap[list(key)[1]]
            assignment[key] = squeezeArc(key, column, assignment)
            boxKey = cellToContainerMap[key][2]
            box = boxToCellMap[boxKey]
            assignment[key] = squeezeArc(key, box, assignment)
    
    return assignment

#Minimum Values Remaining optimization
def sortByMRV(assignment):
    assignmentCopy = OrderedDict()
    assignmentByNumberofValues = {}
    for key, value in assignment.iteritems():
        assignmentByNumberofValues[key] = len(value)
    
    #How to sort a dict by value
    #https://www.saltycrane.com/blog/2007/09/how-to-sort-python-dictionary-by-keys/
    for key, value in sorted(assignmentByNumberofValues.iteritems(), key=lambda (k,v): (v,k)):
        assignmentCopy[key] = assignment[key]
    
    return assignmentCopy

def backtrack(assignment, failedConfigs):
    #printSudoku(assignment)

    """
    if getStringFromDict(assignment) in failedConfigs:
        return assignment, False
    """
    
    assignment = squeezeDomains(assignment)
    #printSudoku(assignment)
    
    if isSolutionValid(assignment):
        return assignment, True
    
    
    unassignedVariable = None
    domain = None
    
    
    assignment = sortByMRV(assignment)
    
    
    for key, value in assignment.iteritems():
        if len(value) != 1:
            unassignedVariable = key
            domain = value
            break
    
    #Ref: https://stackoverflow.com/questions/53513/how-do-i-check-if-a-list-is-empty    
    if domain:
        domainCopy = list(domain)
        
        for x in domain:
            assignmentCopy = assignment.copy()
            assignmentCopy[unassignedVariable] = [x]
            
            if isSudokuConsistent(assignmentCopy):
                #print("key:" + key + ",value:" + x)
                assignmentCopy, result = backtrack(assignmentCopy, failedConfigs)
                if not result:
                    continue
                else:
                    return assignmentCopy, result
            else:
                domainCopy.remove(x)
                assignment[unassignedVariable] = domainCopy    
        
        #revert the assignment for the current variable since it didn't work
        assignment[unassignedVariable] = domain
        
    failedConfigs.add(getStringFromDict(assignment))
    return assignment, False
    

def writeOutput(output):
    print(output)
    file = open("output.txt","a") 
    #Ref: https://stackoverflow.com/questions/6159900/correct-way-to-write-line-to-file
    file.write(output + "\n")
    file.close()

def printSudoku(sudokuDict):
    print("#####################################################################################################")
    i = 1
    stringToPrint = ""
    for key in sorted(cellToContainerMap.iterkeys()):
        stringToPrint = stringToPrint + "".join(sudokuDict[key])
        for j in range (13 - len(sudokuDict[key])):
            stringToPrint = stringToPrint + " "
        
        if i%9 == 0:
            print( stringToPrint + "\n" )
            stringToPrint = ""
        
        i = i + 1
    
#This is the initial sudoku state which needs to be solved            
input_string = sys.argv[1]

print("input string: " + input_string)

#This hold the initial sudoku state in an OrderedDict
sudokuDict = getDict(input_string)


#This is a container of possible consistent sudoku states which we will iterate over to find a solution, the key is the same dict in string format
arcQueueDict = getArcQueue(sudokuDict)


solutionFound = AC3(arcQueueDict, sudokuDict)


failedConfigs = set(())


if solutionFound:
    writeOutput(getStringFromDict(sudokuDict) + " AC3")
else:
    #important to re-initialize the problem from the initial input string
    assignment, result = backtrack(getDict(input_string), failedConfigs)
    
    if result:
        writeOutput(getStringFromDict(assignment) + " BTS")
    else:
        writeOutput("No solution found")
"""

assignment, result = backtrack(getDict(input_string), failedConfigs)
    
if result:
    print(getStringFromDict(assignment))
else:
    print("No solution found")
"""