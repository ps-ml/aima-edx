import pandas as pd
import os
import sys
import string
from HTMLParser import HTMLParser
import time
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import SGDClassifier

train_path = "../resource/lib/publicdata/aclImdb/train/" # use terminal to ls files under this directory
#train_path = "/home/saraswat/machinelearning/data/edx_aima_imbd_week11/train/" # use terminal to ls files under this directory

test_path = "../resource/asnlib/public/imdb_te.csv" # test data for grade evaluation
#test_path = "/home/saraswat/machinelearning/data/edx_aima_imbd_week11/imdb_te.csv" # test data for grade evaluation

def createCombinedFilteredInputModel(stop_words):
    #vocabulary = []
    #Ref: https://stackoverflow.com/questions/3207219/how-do-i-list-all-files-of-a-directory
    filesWithPositiveReviews = os.listdir(train_path + "pos/")
    
    #Ref: https://stackoverflow.com/questions/13784192/creating-an-empty-pandas-dataframe-then-filling-it
    col_names =  ['text', 'polarity']
    combinedFilteredDF  = pd.DataFrame(columns = col_names)
    
    for i in range(len(filesWithPositiveReviews)):
        file = open(train_path + "pos/" + filesWithPositiveReviews[i], "r")
        text = file.read()
        file.close()
        filteredTextList = getFilteredTextList(text, stop_words)
        #Ref: https://stackoverflow.com/questions/8177079/python-take-the-content-of-a-list-and-append-it-to-another-list
        #Ref: https://stackoverflow.com/questions/1306631/python-add-list-to-set
        #vocabulary.extend(filteredTextList)
        combinedFilteredDF.loc[len(combinedFilteredDF)] = [' '.join(filteredTextList), 1]
         
    filesWithNegativeReviews = os.listdir(train_path + "neg/")
    
    for i in range(len(filesWithNegativeReviews)):
        file = open(train_path + "neg/" + filesWithNegativeReviews[i], "r")
        text = file.read()
        file.close()
        filteredTextList = getFilteredTextList(text, stop_words)
        #Ref: https://stackoverflow.com/questions/1306631/python-add-list-to-set
        #vocabulary.extend(filteredTextList)
        #Ref: https://stackoverflow.com/questions/12309976/how-do-i-convert-a-list-into-a-string-with-spaces-in-python
        combinedFilteredDF.loc[len(combinedFilteredDF)] = [' '.join(filteredTextList), 0]

    return combinedFilteredDF

def createFilteredTestModel(stop_words):
    testDF = pd.read_csv(test_path)
    
    for i in range(len(testDF)):
        filteredTextList = getFilteredTextList(testDF.iloc[i]['text'], stop_words)
        testDF.at[i,'text'] = ' '.join(filteredTextList)
    
    return testDF

    
def createUnigramModel(combinedFilteredDF, tfidf):
    #Ref: https://stackoverflow.com/questions/22205845/efficient-way-to-create-term-density-matrix-from-pandas-dataframe
    #Ref: https://stackoverflow.com/questions/15899861/efficient-term-document-matrix-with-nltk
    #Ref: http://scikit-learn.org/stable/modules/feature_extraction.html
    #Ref: http://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.CountVectorizer.html        
    countvec = None
    if tfidf:
        countvec = TfidfVectorizer(max_features=5000)
    else:
        countvec = CountVectorizer(max_features=5000)
        
    #Ref: https://stackoverflow.com/questions/22341271/get-list-from-pandas-dataframe-column
    X = countvec.fit_transform(combinedFilteredDF['text'].tolist())
    features = countvec.get_feature_names()
    
    df = pd.DataFrame(X.toarray(), columns=features)
    
    #print(df.head(5))

    #deleteFile(outputFilename)
    #Ref: https://stackoverflow.com/questions/16923281/pandas-writing-dataframe-to-csv-file
    #combinedFilteredDF.to_csv(outputFilename)
    return df, combinedFilteredDF['polarity'].tolist(), features

def createBigramModel(combinedFilteredDF, tfidf):
    #Ref: https://stackoverflow.com/questions/22205845/efficient-way-to-create-term-density-matrix-from-pandas-dataframe
    #Ref: https://stackoverflow.com/questions/15899861/efficient-term-document-matrix-with-nltk
    #Ref: http://scikit-learn.org/stable/modules/feature_extraction.html
    #Ref: http://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.CountVectorizer.html        
    countvec = None
    if tfidf:
        countvec = TfidfVectorizer(ngram_range=(1,2), max_features=5000)
    else:
        countvec = CountVectorizer(ngram_range=(1,2), max_features=5000)
    #Ref: https://stackoverflow.com/questions/22341271/get-list-from-pandas-dataframe-column
    X = countvec.fit_transform(combinedFilteredDF['text'].tolist())
    features = countvec.get_feature_names()
    #print("features length:" + str(len(features)))
    
    df = pd.DataFrame(X.toarray(), columns=features)
    
    """
    pd.set_option('display.max_colwidth', -1)
    print(df.iloc[[1563]])
    print(combinedFilteredDF.iloc[[1563]])
    print(df.iloc[[696]])
    print(combinedFilteredDF.iloc[[696]])
    print(df.iloc[[250]])
    print(combinedFilteredDF.iloc[[250]])
    print(df.iloc[[104]])
    print(combinedFilteredDF.iloc[[104]])
    """
    #print(df.head(5))

    #deleteFile(outputFilename)
    #Ref: https://stackoverflow.com/questions/16923281/pandas-writing-dataframe-to-csv-file
    #combinedFilteredDF.to_csv(outputFilename)
    return df, combinedFilteredDF['polarity'].tolist(), features

def getStopWords():
    #Ref: https://stackoverflow.com/questions/3277503/in-python-how-do-i-read-a-file-line-by-line-into-a-list
    #Ref: https://stackoverflow.com/questions/4060221/how-to-reliably-open-a-file-in-the-same-directory-as-a-python-script
    lines = [line.rstrip('\n') for line in open(os.path.join(sys.path[0], 'stopwords.en.txt'))]
    
    #Ref: https://stackoverflow.com/questions/15768757/how-to-construct-a-set-out-of-list-items-in-python
    stop_words = set(lines)
    return stop_words


def getFilteredTextList(inputText, stop_words):
    #Remove HTML characters : https://stackoverflow.com/questions/753052/strip-html-from-strings-in-python    
    s = MLStripper()
    s.feed(inputText)
    inputText = s.get_data()
    
    #Remove punctuation characters: https://stackoverflow.com/questions/15547409/how-to-get-rid-of-punctuation-using-nltk-tokenizer
    inputText = inputText.translate(None, string.punctuation)
    
    #Removed digits also
    inputText = inputText.translate(None, string.digits)
    
    #Remove non-UTF-8 characters
    inputText = inputText.decode('utf-8','ignore').encode("utf-8")
    
    #convert everything to lowercase
    inputText = inputText.lower()
    
    #Ref: https://stackoverflow.com/questions/743806/how-to-split-a-string-into-a-list: https://stackoverflow.com/questions/26541968/delete-every-non-utf-8-symbols-from-string
    word_tokens = inputText.split()
    filtered_sentence = []
     
    for w in word_tokens:
        #Ref: https://stackoverflow.com/questions/3627784/case-insensitive-in-python
        if w.lower() not in stop_words:
            filtered_sentence.append(w)
     
    return filtered_sentence

"""
def getXTrain(X, vocabularyList):
    start = time.clock()
    X_train  = pd.DataFrame(columns = vocabularyList)
    
    for i in range(len(X)):
        for j in range( len(vocabularyList)):
            if vocabularyList[j] in set(X.iloc[i]['text'].split()):
                X_train.at[i,vocabularyList[j]] = 1
            else :
                X_train.at[i,vocabularyList[j]] = 0
        print("i:" + str(i) )
    end = time.clock()
    
    #9146 seconds ~152 minutes ~2.5 hours
    print("getXTrain:" + str(end-start))            
    return X_train
"""

def deleteFile(filename):
    try:
        os.remove(filename)
    except OSError:
        pass

"""
def refineVocabulary(vocabularyList):
    start = time.clock()
    refinedVocabularyList = []
    vocabularyDict = {}
    #Ref: https://stackoverflow.com/questions/1602934/check-if-a-given-key-already-exists-in-a-dictionary
    for i in range(len(vocabularyList)):
        if vocabularyList[i] in vocabularyDict:
            vocabularyDict[vocabularyList[i]] += 1
        else:
            vocabularyDict[vocabularyList[i]] = 1
    
    #Ref: https://stackoverflow.com/questions/3294889/iterating-over-dictionaries-using-for-loops        
    for key, value in vocabularyDict.iteritems():
        if value > 1000:
            refinedVocabularyList.append(key)
    
    end = time.clock()
    
    #0.5 seconds
    print("refineVocabulary:" + str(end-start))
    
    return refinedVocabularyList
"""

class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.fed = []
    def handle_data(self, d):
        self.fed.append(d)
    def get_data(self):
        return ''.join(self.fed)


def generatePredictions(X_train,y, features, testDF, testOutputFileName, tfidf):
    start = time.clock()
    #Ref: http://scikit-learn.org/stable/modules/sgd.html
    clf = SGDClassifier(loss="hinge", penalty="l1")
    clf.fit(X_train, y)
    
    countvec = None
    if tfidf:
        countvec = TfidfVectorizer(vocabulary=features)
    else:
        countvec = CountVectorizer(vocabulary=features)
    
    
    X_test = countvec.fit_transform(testDF['text'].tolist())
    
    df = pd.DataFrame(X_test.toarray(), columns=features)
    
    results = clf.predict(df)
    
    deleteFile(testOutputFileName)
    
    file = open(testOutputFileName,"w")
    
    for j in range(len(results)):
        file.write(str(results[j]) + "\n")
    
    file.close()
        
    end = time.clock()
    
    #7 seconds
    print("generatePredictions:" + str(end-start))

outputFilename = os.path.join(sys.path[0],'imdb_tr.csv')
    
stop_words = getStopWords()

combinedFilteredDF = createCombinedFilteredInputModel(stop_words)

testDF = createFilteredTestModel(stop_words)


X_train,y, features = createUnigramModel(combinedFilteredDF, False)

generatePredictions(X_train,y, features, testDF, os.path.join(sys.path[0],'unigram.output.txt'), False)

X_train_b,y_b, features_b = createBigramModel(combinedFilteredDF, False)

generatePredictions(X_train_b,y_b, features_b, testDF, os.path.join(sys.path[0],'bigram.output.txt'), False)

X_train_tfidf,y_tfidf, features_ifidf = createUnigramModel(combinedFilteredDF, True)

generatePredictions(X_train_tfidf,y_tfidf, features_ifidf, testDF, os.path.join(sys.path[0],'unigramtfidf.output.txt'), True)


X_train_tfidf_b,y_tfidf_b, features_ifidf_b = createBigramModel(combinedFilteredDF, True)

generatePredictions(X_train_tfidf_b,y_tfidf_b, features_ifidf_b, testDF, os.path.join(sys.path[0],'bigramtfidf.output.txt'), True)


"""
#There is no index in a set, if we need indexes to encode the documents then we need a list
vocabularyList = list(vocabulary)

refinedVocabularyList = refineVocabulary(vocabularyList)

print("refinedVocabularyList Length:" + str(len(refinedVocabularyList)))
"""
"""
 

deleteFile("output.txt")
file = open("output.txt","w")
for i in range(len(refinedVocabularyList)): 
    file.write(str(refinedVocabularyList[i]) + "\n")
    
file.close
"""
"""
data = pd.read_csv(outputFilename)

y = data.polarity
X = data.drop('polarity', axis=1)

X_train = getXTrain(X, refinedVocabularyList)
"""