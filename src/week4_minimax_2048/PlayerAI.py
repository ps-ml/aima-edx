import math
import time
from timeit import default_timer as timer
from random import randint
from Vector import Vector
from Result import Result
from Cell import Cell
from FarthestPosition import FarthestPosition
from Candidate import Candidate
from Score import Score
from collections import OrderedDict

from BaseAI import BaseAI


class PlayerAI(BaseAI):

    def __init__(self):
        self.vectors = OrderedDict()
        #up
        self.vectors[0] = Vector(0, -1)
        #right
        self.vectors[1] = Vector(1, 0)
        #down
        self.vectors[2] = Vector(0, 1)
        #left
        self.vectors[3] = Vector(-1, 0)
        
        self.deadline = 260
        self.startTime = time.clock()
    
    def elapsed(self, start, source):
        end = timer()
        elapsed = (end - start)*1000 
        #print(source + ":" + str(elapsed) )
    
    #This is returning correct values as compared to ovolve
    def withinBounds(self, grid, position):
        return position.x >= 0 and position.x < grid.size and position.y >= 0 and position.y < grid.size
    
    #This is returning correct values as compared to ovolve
    def findFarthestPosition(self, grid, cell, vector):
        previous = None
        
        #Progress towards the vector direction until an obstacle is found
        #Ref: https://stackoverflow.com/questions/743164/emulate-a-do-while-loop-in-python
        while True:
            previous = cell;
            cell     =  Cell(previous.x + vector.x, previous.y + vector.y)
            pass_condition = self.withinBounds(grid, cell) and grid.getCellValue([cell.x, cell.y]) == 0
            if not pass_condition:
                break
        
       
        return FarthestPosition(previous, cell)
    
    #This is returning correct values as compared to ovolve
    def smoothness(self,grid):
        smoothness = 0;
        for x in range (4):
            for y in range (4):
                cellValue = int(grid.getCellValue([x,y]));
                if  cellValue != 0 :
                    value = math.log(cellValue) / math.log(2);
                    for direction in range (1,3) :
                        vector = self.vectors[direction]
                        targetCell = self.findFarthestPosition(grid, Cell(x,y), vector).next;
                        
                        target = grid.getCellValue([targetCell.x, targetCell.y])
                        if target != None : 
                            targetValue = math.log(target)/math.log(2);
                            smoothness -= abs(value - targetValue);
            
        
        return smoothness;
    
    
    #This is returning correct values as compared to ovolve
    def monotonicity(self, grid):
        #scores for all four directions
        totals = [0, 0, 0, 0]
        
        #up/down direction
        for x in range(4) :
            current = 0
            next = current + 1
            while next < 4 :
                while next < 4 and grid.getCellValue([x,next]) == 0 :
                    next = next + 1
          
                if next >= 4:
                    next = next - 1
                
                currentValue = 0
                xcurrentValue = grid.getCellValue([x,current])
                if  xcurrentValue != 0 :
                    currentValue = math.log(xcurrentValue)/math.log(2)  
                
                nextValue = 0
                xnextValue = grid.getCellValue([x,next])
                if xnextValue != 0 :
                    nextValue = math.log(xnextValue)/math.log(2)
                
                
                if currentValue > nextValue :
                    totals[0] = totals[0] + nextValue - currentValue;
                elif nextValue > currentValue :
                    totals[1] += currentValue - nextValue;
                
                current = next
                next = next + 1
    
    
        # left/right direction
        for y in range(4) :
            current = 0;
            next = current + 1;
            while  next < 4 :
                while next < 4 and grid.getCellValue([next,y]) == 0 :
                    next = next + 1
                
                if next >= 4:
                        next = next - 1
                        
                currentValue = 0
                ycurrentValue = grid.getCellValue([current,y])
                if  ycurrentValue != 0 :
                        currentValue = math.log(ycurrentValue)/math.log(2)
                        
                nextValue = 0
                ynextValue = grid.getCellValue([next,y])
                if ynextValue != 0 :
                    nextValue = math.log(ynextValue)/math.log(2)    
              
                if currentValue > nextValue : 
                    totals[2] += nextValue - currentValue;
                elif nextValue > currentValue :
                    totals[3] += currentValue - nextValue;
              
                current = next
                next = next + 1
        
      
        return max(totals[0], totals[1]) + max(totals[2], totals[3]);            
    
    #This is returning correct values as compared to ovolve
    def evaluateBoard(self, grid):
        availableCells = len(grid.getAvailableCells());
    
        smoothWeight = 0.1
        monotonicityWeight  = 1.0
        emptyWeight  = 2.7
        maxWeight    = 1.0
        
        maxValue = math.log(grid.getMaxTile()) / math.log(2)
        
        
        #boardEvaluation = self.smoothness(grid)*smoothWeight + self.monotonicity(grid)*monotonicityWeight + math.log(availableCells) * emptyWeight + grid.getMaxTile() *  maxWeight;
        boardEvaluation = self.smoothness(grid)*smoothWeight + self.monotonicity(grid)*monotonicityWeight + math.log(availableCells) * emptyWeight + maxValue *  maxWeight;
        #boardEvaluation = self.monotonicity(grid)*monotonicityWeight + math.log(availableCells) * emptyWeight + grid.getMaxTile() *  maxWeight;
        
        return boardEvaluation
    
    #This is returning a correct result
    def isWin(self, grid):
        for x in range (4):
            for y in range (4):
                if grid.getCellValue([x,y]) == 2048:
                    return True
      
        return False;
    
    #Get the vector representing the chosen direction
    def getVector(self, direction) :
        #Vectors representing tile movement
        return self.vectors[direction];
    
    def mark(self, x, y, marked, grid, tuplexy):
        if x >= 0 and x <= 3 and y >= 0 and y <= 3 and grid.getCellValue([x,y]) != 0 and tuplexy not in marked :
            marked.add(tuplexy)
          
            for direction in range(4):
                vector = self.getVector(direction);
                self.mark(x + vector.x, y + vector.y, marked, grid)
    
    #counts the number of isolated groups.
    # for the ovolve code this doesn't really work, they are simply returning the number of populated cells, which is always equal for any number of variations of a grid 
    def islands(self, grid):
        islands = 0;
        marked = set(())
        
        for x in range(4) :
            for y in range(4) :
                tuplexy = tuple([x,y])
                if tuple([x,y]) not in marked:
                    islands = islands + 1
                    self.mark(x, y , marked, grid, tuplexy);
                    
        return islands;
    
    
    
    #This is returning correct values as compared to ovolve for both ai and computer move
    # alpha-beta depth first search
    def search(self, grid, playerTurn, depth, alpha, beta, start):
        if time.clock() - start > 0.15 :
            depth = 0
            
        bestScore = None
        bestMove = -1
        result = None
        
        #the maxing player
        if playerTurn :
            bestScore = alpha;
            moves = grid.getAvailableMoves()
            for direction in moves :
                newGrid = grid.clone()
                newGrid.move(direction)
                
                if newGrid.getMaxTile() == 2048 :
                    return Result( direction, 10000 )
                
                if depth == 0 :
                    #self.elapsed(start,"playerTurn")
                    result = Result( direction, self.evaluateBoard(newGrid))
                else:
                    result = self.search(newGrid, False, depth - 1, bestScore, beta,start)
                    if result.score > 9900 : 
                        # to slightly penalize higher depth from win
                        result.score = result.score - 1
                      
                if result.score > bestScore :
                    bestScore = result.score;
                    bestMove = direction;
                
                #tried this with >= but didn't speed up the process or yield better results
                if bestScore > beta :
                    #self.elapsed(start,"playerTurn")
                    return Result(bestMove, beta)
                
              
            
      
        #computer's turn, we'll do heavy pruning to keep the branching factor low
        else : 
            bestScore = beta;
        
            # try a 2 and 4 in each cell and measure how annoying it is
            # with metrics from eval
            candidates = [];
            cells = grid.getAvailableCells()
            
            """
            This was an attempt to return to insert a random cell and return an evaluation. 
            This gives max tile of 64. Note that we have to insert a cell here and not make a move
            randomCell = cells[randint(0, len(cells) - 1)]
            newGrid = grid.clone()
            newGrid.setCellValue(randomCell, 2)
            
            #also tried returning board evaluation here
            return Result(bestMove, alpha + 1)
            
            """
            score2 = Score(2, -5, None)
            score4 = Score(4, -5, None)
            scoresList = [score2, score4]
            newGrid = grid.clone()
            
            for score in scoresList :
                for i, cell in enumerate(cells) :
                    newGrid.setCellValue(cell, score.value)
                    #evaluation = -self.smoothness(newGrid) + self.islands(newGrid)
                    evaluation = -self.smoothness(newGrid) + 16 - len(newGrid.getAvailableCells())
                    #evaluation = self.islands(newGrid)
                    if evaluation > score.score :
                        score.score = evaluation
                        score.position = cell
                    
                    #revert the value, this is an optimization so that we don't keep cloning the grid which is expensive
                    newGrid.setCellValue(cell, 0) 
        
            #now just pick out the most annoying moves
            maxScore = max( score2.score, score4.score ) 
            
            #2 and 4
            for score in scoresList :
                if score.score == maxScore :
                    candidates.append( Candidate(score.position, score.value) )
        
            #search on each candidate
            for candidate in candidates :
                position = candidate.position
                value = candidate.value;
                newGrid.setCellValue( position, value)
                playerTurn = True
                result = self.search(newGrid, True, depth, alpha, bestScore,start)
            
                if result.score < bestScore :
                    bestScore = result.score;
                
                if bestScore < alpha :
                    return Result( None, alpha)
            
            
        #self.elapsed(start, str(playerTurn))
        return Result( bestMove, bestScore)
    
    def createTestGrid(self, grid):
        grid.setCellValue((0,0), 0)
        grid.setCellValue((0,1), 0)
        grid.setCellValue((0,2), 0)
        grid.setCellValue((0,3), 8)
        grid.setCellValue((1,0), 2)
        grid.setCellValue((1,1), 0)
        grid.setCellValue((1,2), 8)
        grid.setCellValue((1,3), 16)
        grid.setCellValue((2,0), 2)
        grid.setCellValue((2,1), 4)
        grid.setCellValue((2,2), 16)
        grid.setCellValue((2,3), 64)
        grid.setCellValue((3,0), 4)
        grid.setCellValue((3,1), 8)
        grid.setCellValue((3,2), 64)
        grid.setCellValue((3,3), 256)
        return grid
    
    def getMove(self, grid):
        start = time.clock()
        #print(start)
        depth = 10
        #grid = self.createTestGrid(grid)
        #print(self.search(grid, True, depth, -10000, 10000).move)
        moves = grid.getAvailableMoves()
        newResult = None
        
        """
        end = time.clock()
        
        
        if (end - self.startTime) > 260:
            print(end)
            return None
        """     
        
        #attempted to increase the timer but even 0.06 doesn't work
        #while time.clock() - start < 0.05 and grid.getMaxTile() != 2048:
        #print(time.clock())
        newResult = self.search(grid, True, depth, -10000, 10000, start);
        #depth = depth + 1
            #print(depth)
        
        if newResult != None and newResult.move != -1:
            return newResult.move
        else :
            return moves[0]
        
        """
        #return moves[randint(0, len(moves) - 1)] if moves else None
        """    
