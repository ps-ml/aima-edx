class BoardState:
    def __init__(self, state, parent, action, depth, astValue):
        self.state = state
        self.parent = parent
        self.action = action
        self.depth = depth
        self.astValue = astValue