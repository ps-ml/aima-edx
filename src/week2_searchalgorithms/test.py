from collections import OrderedDict


#frontier = []
#state = ['0','1','2','3','4','5','6','7','8']
#newBoardState = BoardState(state, None, 'Right', 0, 0)

#frontier.append(newBoardState)

#if newBoardState in frontier:
    #print("found")



frontier = OrderedDict()
state1 = ['6','0','8','4','1','2','7','3','5']
state2 = ['6','1','8','4','3','2','7','0','5']
state3 = ['6','1','8','0','4','2','7','3','5']
state4 = ['6','1','8','4','2','0','7','3','5']

frontier[tuple(state1)] = "1"
frontier[tuple(state2)] = "2"
frontier[tuple(state3)] = "3"
frontier[tuple(state4)] = "4"

for key, value in frontier.iteritems():
    print(key)
    print(value)
