import sys
import Queue
import time
#Ref: https://stackoverflow.com/questions/4534438/typeerror-module-object-is-not-callable
from BoardState import BoardState
from collections import OrderedDict

#import resource

#Ref: https://stackoverflow.com/questions/3620943/measuring-elapsed-time-with-the-time-module
start_time = time.time()

#Ref: https://stackoverflow.com/questions/25605380/passing-directory-to-python-script-as-command-line-argument/25605529#25605529
searchMethod = sys.argv[1]

#Ref: https://stackoverflow.com/questions/7844118/how-to-convert-comma-delimited-string-to-list-in-python
initialState = sys.argv[2].split(",")

#Ref: https://stackoverflow.com/questions/31467147/how-to-declare-uninitialized-variable-in-class-definition-in-python
initialBoardState = BoardState(list(initialState), None, None, 0, 0)

#Ref: https://stackoverflow.com/questions/2972212/creating-an-empty-list-in-python
path_to_goal = []
#Ref: https://stackoverflow.com/questions/1748641/how-do-i-use-a-boolean-in-python
goalFound = 0

nodes_expanded = 0

search_depth = 0

max_search_depth = 0
#print(initialState)

#Ref: http://mypuzzle.org/sliding
#Ref: https://www.programiz.com/python-programming/array
goalState=['0','1','2','3','4','5','6','7','8']

#Ref: https://www.w3schools.com/python/python_sets.asp
explored = set(())

finalBoardState = None

def runBFS():
    #Ref: http://www.learn4master.com/programming-language/python/python-queue-for-multithreading
    #By Default this is a FIFO queue. Ref: https://docs.python.org/2/library/queue.html
    frontier = Queue.Queue()
    #This set is needed because searching in a set is an O(1) operation. Searching in a queue/list is an O(n) operation
    frontierSet = set(())
    frontierBoard = Queue.Queue();
    #https://stackoverflow.com/questions/2612802/how-to-clone-or-copy-a-list
    frontier.put(initialBoardState.state)
    frontierSet.add(tuple(initialBoardState.state))
    #This hack is needed because when we eventually find the solution, we need to print path_to_goal. This requires parent information
    #for the state. The only way I know how to store the parent and the list together is in a class. This is the BoardState class.
    #However, we need to put the BoardState in a queue/set etc. This would require me to override equals/hashcode etc which is hairy in
    #python. So I am maintaining a parallel queue.
    frontierBoard.put(initialBoardState)
    
    #Ref: https://www.w3schools.com/python/python_while_loops.asp
    while frontier.not_empty :
        state = frontier.get()
        tupleState = tuple(state)
        frontierSet.remove(tupleState)
        frontierBoardState = frontierBoard.get()
        
        explored.add(tupleState)
        
        #Ref: https://stackoverflow.com/questions/36420022/how-can-i-compare-two-ordered-lists-in-python
        if state == goalState:
            global goalFound
            goalFound = 1
            finalBoardState = frontierBoardState
            #Ref: https://www.programiz.com/python-programming/break-continue
            break
        
        #Ref: https://stackoverflow.com/questions/1485841/behaviour-of-increment-and-decrement-operators-in-python
        #Ref: https://stackoverflow.com/questions/11904981/local-variable-referenced-before-assignment
        global nodes_expanded
        nodes_expanded = nodes_expanded + 1
        
        currentSearchDepth = frontierBoardState.depth + 1;
        
        global max_search_depth
        if currentSearchDepth > max_search_depth:
            max_search_depth = currentSearchDepth
        
        #Ref: https://www.programiz.com/python-programming/methods/list/index
        index0 = state.index('0')
        #Up
        if (index0 - 3) >= 0:
            #Ref: https://stackoverflow.com/questions/2493920/how-to-switch-position-of-two-items-in-a-python-list
            indexUp = index0 - 3;
            newStateUp = list(state)
            newStateUp[index0], newStateUp[indexUp] = newStateUp[indexUp], newStateUp[index0]
            #Apparently the switch must be done on one line, two lines doesn't work
            #newStateUp[index0] = state[indexUp]
            #newStateUp[indexUp] = state[index0]
            #Ref: https://stackoverflow.com/questions/27024881/how-to-check-if-object-exists-in-queue-before-pushing-new-object
            #https://www.programiz.com/python-programming/operators
            tupleNewStateUp = tuple(newStateUp)
            if(tupleNewStateUp not in explored and tupleNewStateUp not in frontierSet):
                frontier.put(newStateUp)
                frontierSet.add(tupleNewStateUp)
                newBoardState = BoardState(list(newStateUp), frontierBoardState, 'Up', currentSearchDepth, 0)
                frontierBoard.put(newBoardState)
        
        #Down    
        if (index0 + 3) < 9:
            indexDown = index0 + 3;
            newStateDown = list(state)
            newStateDown[index0], newStateDown[indexDown] = newStateDown[indexDown], newStateDown[index0]
            tupleNewStateDown = tuple(newStateDown)
            if( tupleNewStateDown not in explored and tupleNewStateDown not in frontierSet):
                frontier.put(newStateDown)
                frontierSet.add(tupleNewStateDown)
                newBoardState = BoardState(list(newStateDown), frontierBoardState, 'Down', currentSearchDepth, 0)
                frontierBoard.put(newBoardState)
    
        #Left, Ref: https://stackoverflow.com/questions/4432208/how-does-work-in-python    
        if (index0%3) != 0:
            indexLeft = index0 -1;
            newStateLeft = list(state)
            newStateLeft[index0], newStateLeft[indexLeft] = newStateLeft[indexLeft], newStateLeft[index0]
            tupleNewStateLeft = tuple(newStateLeft)
            if(tupleNewStateLeft not in explored and tupleNewStateLeft not in frontierSet):
                frontier.put(newStateLeft)
                frontierSet.add(tupleNewStateLeft)
                newBoardState = BoardState(list(newStateLeft), frontierBoardState, 'Left', currentSearchDepth, 0)
                frontierBoard.put(newBoardState)
            
        #Right    
        if (index0%3) != 2:
            indexRight = index0 + 1;
            newStateRight = list(state)
            newStateRight[index0], newStateRight[indexRight] = newStateRight[indexRight], newStateRight[index0]
            tupleNewStateRight = tuple(newStateRight)
            if(tupleNewStateRight not in explored and tupleNewStateRight not in frontierSet):
                frontier.put(newStateRight)
                frontierSet.add(tupleNewStateRight)
                newBoardState = BoardState(list(newStateRight), frontierBoardState, 'Right', currentSearchDepth, 0)
                frontierBoard.put(newBoardState)
    
    global search_depth        
    search_depth = finalBoardState.depth
    while finalBoardState.parent != None:
        path_to_goal.append(finalBoardState.action)
        finalBoardState = finalBoardState.parent;
            
    #Ref: https://stackoverflow.com/questions/3940128/how-can-i-reverse-a-list-in-python        
    path_to_goal.reverse()


def runDFS():
    #LIFO queue. Ref: https://docs.python.org/2/library/queue.html
    #frontier = Queue.LifoQueue()
    #frontierBoard = Queue.LifoQueue();
    frontier = []
    frontierSet = set(())
    frontierBoard = []
    #https://stackoverflow.com/questions/2612802/how-to-clone-or-copy-a-list
    frontier.append(initialBoardState.state)
    frontierSet.add(tuple(initialBoardState.state))
    #This hack is needed because when we eventually find the solution, we need to print path_to_goal. This requires parent information
    #for the state. The only way I know how to store the parent and the list together is in a class. This is the BoardState class.
    #However, we need to put the BoardState in a queue/set etc. This would require me to override equals/hashcode etc which is hairy in
    #python. So I am maintaining a parallel queue.
    frontierBoard.append(initialBoardState)
    
    #Ref: https://www.w3schools.com/python/python_while_loops.asp
    while len(frontier) > 0 :
        state = frontier.pop()
        tupleState = tuple(state)
        frontierSet.remove(tupleState)
        frontierBoardState = frontierBoard.pop()
        
        explored.add(tupleState)
        
        #Ref: https://stackoverflow.com/questions/36420022/how-can-i-compare-two-ordered-lists-in-python
        if state == goalState:
            global goalFound
            goalFound = 1
            finalBoardState = frontierBoardState
            #Ref: https://www.programiz.com/python-programming/break-continue
            break
        
        #Ref: https://stackoverflow.com/questions/11904981/local-variable-referenced-before-assignment
        global nodes_expanded
        nodes_expanded = nodes_expanded + 1
        
        currentSearchDepth = frontierBoardState.depth;
        
        global max_search_depth
        if currentSearchDepth > max_search_depth:
            max_search_depth = currentSearchDepth
        
        #Ref: https://www.programiz.com/python-programming/methods/list/index
        index0 = state.index('0')
        #Right    
        if (index0%3) != 2:
            indexRight = index0 + 1;
            newStateRight = list(state)
            newStateRight[index0], newStateRight[indexRight] = newStateRight[indexRight], newStateRight[index0]
            tupleNewStateRight = tuple(newStateRight)
            if( tupleNewStateRight not in explored and tupleNewStateRight not in frontierSet):
                frontier.append(newStateRight)
                frontierSet.add(tupleNewStateRight)
                newBoardState = BoardState(list(newStateRight), frontierBoardState, 'Right', frontierBoardState.depth + 1, 0)
                frontierBoard.append(newBoardState)
        
        #Left, Ref: https://stackoverflow.com/questions/4432208/how-does-work-in-python    
        if (index0%3) != 0:
            indexLeft = index0 -1;
            newStateLeft = list(state)
            newStateLeft[index0], newStateLeft[indexLeft] = newStateLeft[indexLeft], newStateLeft[index0]
            tupleNewStateLeft = tuple(newStateLeft)
            if(tupleNewStateLeft not in explored and tupleNewStateLeft not in frontierSet):
                frontier.append(newStateLeft)
                frontierSet.add(tupleNewStateLeft)
                newBoardState = BoardState(list(newStateLeft), frontierBoardState, 'Left', frontierBoardState.depth + 1, 0)
                frontierBoard.append(newBoardState)        
        
        #Down    
        if (index0 + 3) < 9:
            indexDown = index0 + 3;
            newStateDown = list(state)
            newStateDown[index0], newStateDown[indexDown] = newStateDown[indexDown], newStateDown[index0]
            tupleNewStateDown = tuple(newStateDown)
            if( tupleNewStateDown not in explored and tupleNewStateDown not in frontierSet):
                frontier.append(newStateDown)
                frontierSet.add(tupleNewStateDown)
                newBoardState = BoardState(list(newStateDown), frontierBoardState, 'Down', frontierBoardState.depth + 1, 0)
                frontierBoard.append(newBoardState)
                
        #Up
        if (index0 - 3) >= 0:
            #Ref: https://stackoverflow.com/questions/2493920/how-to-switch-position-of-two-items-in-a-python-list
            indexUp = index0 - 3;
            newStateUp = list(state)
            newStateUp[index0], newStateUp[indexUp] = newStateUp[indexUp], newStateUp[index0]
            tupleNewStateUp = tuple(newStateUp)
            if(tupleNewStateUp not in explored and tupleNewStateUp not in frontierSet):
                frontier.append(newStateUp)
                frontierSet.add(tupleNewStateUp)
                newBoardState = BoardState(list(newStateUp), frontierBoardState, 'Up', frontierBoardState.depth + 1, 0)
                frontierBoard.append(newBoardState)
            
        
    global search_depth        
    search_depth = finalBoardState.depth
    if search_depth > max_search_depth:
            max_search_depth = search_depth
    while finalBoardState.parent != None:
        path_to_goal.append(finalBoardState.action)
        finalBoardState = finalBoardState.parent;
            
    #Ref: https://stackoverflow.com/questions/3940128/how-can-i-reverse-a-list-in-python        
    path_to_goal.reverse()


def runAST():
    #add initialboardstate with heuristic cost to dict, state (initialState is the key)
    #create getMinCostState function which will compare states in the frontier and return with the min cost state
    tupleInitialState = tuple(initialState)
    initialBoardState = BoardState(tupleInitialState, None, None, 0, getHeuristic(initialState))
    #frontier = {tupleInitialState : initialBoardState}
    frontier = OrderedDict()
    frontier[tupleInitialState] = initialBoardState
    frontierSet = set(())
    #frontierBoard = []
    #https://stackoverflow.com/questions/2612802/how-to-clone-or-copy-a-list
    #frontier.append(initialBoardState.state)
    frontierSet.add(tupleInitialState)
    #This hack is needed because when we eventually find the solution, we need to print path_to_goal. This requires parent information
    #for the state. The only way I know how to store the parent and the list together is in a class. This is the BoardState class.
    #However, we need to put the BoardState in a queue/set etc. This would require me to override equals/hashcode etc which is hairy in
    #python. So I am maintaining a parallel queue.
    #frontierBoard.append(initialBoardState)
    
    #Ref: https://www.w3schools.com/python/python_while_loops.asp
    #Ref: https://www.tutorialspoint.com/python/dictionary_len.htm
    while len(frontier) > 0 :
        #for key, value in frontier.iteritems():
            #print(key)
            #print(value.astValue)
        
        state = getMinCostState(frontier)
        #Ref: https://stackoverflow.com/questions/5404665/accessing-elements-of-python-dictionary
        frontierBoardState = frontier[state]
        #print(state)
        #print(frontierBoardState.astValue)
        tupleState = tuple(state)
        del frontier[tupleState]
        frontierSet.remove(tupleState)
        
        
        explored.add(tupleState)
        
        #Ref: https://stackoverflow.com/questions/36420022/how-can-i-compare-two-ordered-lists-in-python
        if state == tuple(goalState):
            global goalFound
            goalFound = 1
            finalBoardState = frontierBoardState
            #Ref: https://www.programiz.com/python-programming/break-continue
            break
        
        #Ref: https://stackoverflow.com/questions/11904981/local-variable-referenced-before-assignment
        global nodes_expanded
        nodes_expanded = nodes_expanded + 1
        
        currentSearchDepth = frontierBoardState.depth;
        
        global max_search_depth
        if currentSearchDepth > max_search_depth:
            max_search_depth = currentSearchDepth
        
        #Ref: https://www.programiz.com/python-programming/methods/list/index
        index0 = state.index('0')
        #Up
        if (index0 - 3) >= 0:
            #Ref: https://stackoverflow.com/questions/2493920/how-to-switch-position-of-two-items-in-a-python-list
            indexUp = index0 - 3;
            newStateUp = list(state)
            newStateUp[index0], newStateUp[indexUp] = newStateUp[indexUp], newStateUp[index0]
            tupleNewStateUp = tuple(newStateUp)
            newStateHeuristic = getHeuristic(newStateUp);
            frontierBoardStateDepth = frontierBoardState.depth + 1
            if(tupleNewStateUp not in explored and tupleNewStateUp not in frontierSet):
                newBoardState = BoardState(list(newStateUp), frontierBoardState, 'Up', frontierBoardStateDepth, newStateHeuristic + frontierBoardStateDepth)
                frontier[tupleNewStateUp] = newBoardState
                frontierSet.add(tupleNewStateUp)
            #decrease-key
            elif tupleNewStateUp in frontierSet:
                boardStateInFrontier = frontier[tupleNewStateUp]
                boardStateInFrontierHeuristic = boardStateInFrontier.astValue
                newStateUpHeuristic = newStateHeuristic + frontierBoardStateDepth
                if int(newStateUpHeuristic) < int(boardStateInFrontierHeuristic):
                    del frontier[tupleNewStateUp]
                    newBoardState = BoardState(list(newStateUp), frontierBoardState, 'Up', frontierBoardStateDepth, newStateHeuristic + frontierBoardStateDepth)
                    frontier[tupleNewStateUp] = newBoardState
                    
                
        
        #Down    
        if (index0 + 3) < 9:
            indexDown = index0 + 3;
            newStateDown = list(state)
            newStateDown[index0], newStateDown[indexDown] = newStateDown[indexDown], newStateDown[index0]
            tupleNewStateDown = tuple(newStateDown)
            newStateHeuristic = getHeuristic(newStateDown);
            frontierBoardStateDepth = frontierBoardState.depth + 1
            if( tupleNewStateDown not in explored and tupleNewStateDown not in frontierSet):
                newBoardState = BoardState(list(newStateDown), frontierBoardState, 'Down', frontierBoardStateDepth, newStateHeuristic + frontierBoardStateDepth )
                frontier[tupleNewStateDown] = newBoardState
                frontierSet.add(tupleNewStateDown)
            elif tupleNewStateDown in frontierSet:
                boardStateInFrontier = frontier[tupleNewStateDown]
                boardStateInFrontierHeuristic = boardStateInFrontier.astValue
                newStateDownHeuristic = newStateHeuristic + frontierBoardStateDepth
                if int(newStateDownHeuristic) < int(boardStateInFrontierHeuristic):
                    del frontier[tupleNewStateDown]
                    newBoardState = BoardState(list(newStateDown), frontierBoardState, 'Down', frontierBoardStateDepth, newStateHeuristic + frontierBoardStateDepth)
                    frontier[tupleNewStateDown] = newBoardState    
        
        #Left, Ref: https://stackoverflow.com/questions/4432208/how-does-work-in-python    
        if (index0%3) != 0:
            indexLeft = index0 -1;
            newStateLeft = list(state)
            newStateLeft[index0], newStateLeft[indexLeft] = newStateLeft[indexLeft], newStateLeft[index0]
            tupleNewStateLeft = tuple(newStateLeft)
            newStateHeuristic = getHeuristic(newStateLeft);
            frontierBoardStateDepth = frontierBoardState.depth + 1
            if(tupleNewStateLeft not in explored and tupleNewStateLeft not in frontierSet):
                newBoardState = BoardState(list(newStateLeft), frontierBoardState, 'Left', frontierBoardStateDepth, newStateHeuristic + frontierBoardStateDepth )
                frontier[tupleNewStateLeft] = newBoardState
                frontierSet.add(tupleNewStateLeft)
            elif tupleNewStateLeft in frontierSet:
                boardStateInFrontier = frontier[tupleNewStateLeft]
                boardStateInFrontierHeuristic = boardStateInFrontier.astValue
                newStateLeftHeuristic = newStateHeuristic + frontierBoardStateDepth
                if int(newStateLeftHeuristic) < int(boardStateInFrontierHeuristic):
                    del frontier[tupleNewStateLeft]
                    newBoardState = BoardState(list(newStateLeft), frontierBoardState, 'Left', frontierBoardStateDepth, newStateHeuristic + frontierBoardStateDepth)
                    frontier[tupleNewStateLeft] = newBoardState            

        #Right    
        if (index0%3) != 2:
            indexRight = index0 + 1;
            newStateRight = list(state)
            newStateRight[index0], newStateRight[indexRight] = newStateRight[indexRight], newStateRight[index0]
            tupleNewStateRight = tuple(newStateRight)
            newStateHeuristic = getHeuristic(newStateRight);
            frontierBoardStateDepth = frontierBoardState.depth + 1
            if( tupleNewStateRight not in explored and tupleNewStateRight not in frontierSet):
                newBoardState = BoardState(list(newStateRight), frontierBoardState, 'Right', frontierBoardStateDepth, newStateHeuristic + frontierBoardStateDepth)
                frontier[tupleNewStateRight] = newBoardState
                frontierSet.add(tupleNewStateRight)
            elif tupleNewStateRight in frontierSet:
                boardStateInFrontier = frontier[tupleNewStateRight]
                boardStateInFrontierHeuristic = boardStateInFrontier.astValue
                newStateRightHeuristic = newStateHeuristic + frontierBoardStateDepth
                if int(newStateRightHeuristic) < int(boardStateInFrontierHeuristic):
                    del frontier[tupleNewStateRight]
                    newBoardState = BoardState(list(newStateRight), frontierBoardState, 'Right', frontierBoardStateDepth, newStateHeuristic + frontierBoardStateDepth)
                    frontier[tupleNewStateRight] = newBoardState    
        
    global search_depth        
    search_depth = finalBoardState.depth
    if search_depth > max_search_depth:
            max_search_depth = search_depth
    while finalBoardState.parent != None:
        path_to_goal.append(finalBoardState.action)
        finalBoardState = finalBoardState.parent;
            
    #Ref: https://stackoverflow.com/questions/3940128/how-can-i-reverse-a-list-in-python        
    path_to_goal.reverse()

#number of tiles in incorrect position was the original implementation but the project description requires calculation of manhattan distance
def getHeuristic(state):
    heuristic = 0
    #http://treyhunner.com/2016/04/how-to-loop-with-indexes-in-python/
    for i in range(len(state)):
        #For some reason the command line state is initialized as a list of strings. We need to convert the string to int when comparing to the index
        #Ref: https://stackoverflow.com/questions/642154/how-to-convert-strings-into-integers-in-python
        #if (i != int(state[i])):
            #heuristic = heuristic + 1
        diff = abs(i - int(state[i])) 
        if  diff < 3:
            heuristic = heuristic + diff
        elif diff < 6:
            heuristic = heuristic + diff - 2
        elif diff < 9:
            heuristic = heuristic + diff - 4         
            
    
    return heuristic        

def getMinCostState(frontier):
    #https://stackoverflow.com/questions/30362391/how-do-you-find-the-first-key-in-a-dictionary
    minCostState = next(iter(frontier))
    minCostBoardState = frontier[minCostState]
    #https://stackoverflow.com/questions/3294889/iterating-over-dictionaries-using-for-loops
    for key, value in frontier.iteritems():
        if int(value.astValue) < int(minCostBoardState.astValue):
            minCostState = key
            minCostBoardState = value
    
    return minCostState
    
def printResults():    
    if(goalFound):
        #Ref: http://www.pythonforbeginners.com/files/reading-and-writing-files-in-python
        file = open("output.txt","w") 
        #Ref: https://stackoverflow.com/questions/6159900/correct-way-to-write-line-to-file
        file.write("path_to_goal: " + str(path_to_goal) + "\n")
        #Ref: https://stackoverflow.com/questions/1712227/how-to-get-the-number-of-elements-in-a-list-in-python
        file.write("cost_of_path: " + str(len(path_to_goal)) + "\n")
        file.write("nodes_expanded: " + str(nodes_expanded) + "\n")
        file.write("search_depth: " + str(search_depth) + "\n")
        file.write("max_search_depth: " + str(max_search_depth) + "\n")
        file.write("running_time: " + str(elapsed_time) + "\n")
        file.write("max_ram_usage: 5\n")
        file.close()

if(searchMethod == "bfs"):    
    runBFS();
#Ref: https://www.tutorialspoint.com/python/python_if_else.htm    
elif(searchMethod == "dfs"):
    runDFS();
elif(searchMethod == "ast"):
    runAST();
        
end_time = time.time()

elapsed_time = end_time - start_time

printResults();