import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
from sklearn import svm
from sklearn.ensemble import RandomForestRegressor
from sklearn.pipeline import make_pipeline
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import cross_val_score
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.metrics import accuracy_score
from sklearn.externals import joblib
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
import sys


def writeOutputFile(file, algorithm, best_score, test_score):
    #Ref: https://www.pythonforbeginners.com/files/reading-and-writing-files-in-python
    line = str(algorithm) + "," + str(best_score) + "," + str(test_score) + "\n"
    file.write(line) 

inputFile = sys.argv[1]
outputFile = sys.argv[2]


data = pd.read_csv(inputFile)

y = data.label
X = data.drop('label', axis=1)

X_train, X_test, y_train, y_test = train_test_split(X, y, 
                                                    test_size=0.4, 
                                                    random_state=123, 
                                                    stratify=y)


algorithms = ["svm_linear","svm_polynomial","svm_rbf","logistic","knn","decision_tree","random_forest"]

file = open(outputFile,"w")

for i in range(len(algorithms)):

    if algorithms[i] == "svm_linear" :
        params = {'C':[0.1, 0.5, 1, 5, 10, 50, 100]}
        model = svm.SVC(kernel='linear')
    elif algorithms[i] == "svm_polynomial" :
        params = {'C':[0.1, 1, 3], 'degree':[4,5,6], 'gamma':[0.1,0.5], 'kernel':['poly']}
        model = svm.SVC()
    elif algorithms[i] == "svm_rbf" :
        params = {'C':[0.1, 0.5, 1, 5, 10, 50, 100], 'gamma':[0.1,0.5,1,3,6,10]}
        model = svm.SVC(kernel='rbf')
    elif algorithms[i] == "logistic" :
        params = {'C':[0.1, 0.5, 1, 5, 10, 50, 100]}
        model = svm.LinearSVC()
    elif algorithms[i] == "knn" :
        params = {'n_neighbours':range(1, 50), 'leaf_size':range(5, 60, 5)}
        model = KNeighborsClassifier()
    elif algorithms[i] == "decision_tree" :
        params = {'max_depth':range(1, 50), 'min_samples_split':range(2, 10)}
        model = DecisionTreeClassifier()
    elif algorithms[i] == "random_forest" :
        params = {'max_depth':range(1, 50), 'min_samples_split':range(2, 10)}
        model = RandomForestClassifier()    
    
    GridSearchCV(model, params, cv=5)
    
    model.fit(X_train, y_train)
        
    writeOutputFile(file, algorithms[i], model.score(X_train, y_train), model.score(X_test, y_test))
        
file.close()
    
"""
pipeline = make_pipeline(preprocessing.StandardScaler(), RandomForestRegressor(n_estimators=100))

hyperparameters = { 'randomforestregressor__max_features' : ['auto', 'sqrt', 'log2'],
                  'randomforestregressor__max_depth': [None, 5, 3, 1]}

clf = GridSearchCV(pipeline, hyperparameters, cv=10)


y_pred = model.predict(X_test)

print r2_score(y_test, y_pred)
"""