This folder contains solutions to assignments for this course: https://www.edx.org/course/artificial-intelligence-ai-columbiax-csmm-101x-0

This course covers significant material from the Artificial Intelligence book by Russell and Norvig: https://www.amazon.com/Artificial-Intelligence-Approach-Stuart-Russell-dp-9332543518/dp/9332543518/ref=mt_paperback?_encoding=UTF8&me=&qid=

This folder also contains the problem assignments.
